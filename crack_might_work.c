#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary, char *passwordToReturn)
{
    char readPassword[PASS_LEN];
    char hashFromPassword[HASH_LEN];

    int readPasswordLength, givenHashLength;

    bool isPasswordFound = false;
    
    // Open the dictionary file
    FILE *dictionaryPointer = fopen(dictionary, "r");

    // Loop through the dictionary file, one line
    // at a time.
    while (!feof(dictionaryPointer))
        {
            fgets(readPassword, PASS_LEN, dictionaryPointer);

            for(int stringAddress = 0; stringAddress < PASS_LEN + 1; stringAddress++)
            {
                if(readPassword[stringAddress] == '\n')
                {
                    readPassword[stringAddress] = '\0';
                    readPasswordLength = stringAddress;
                    break;
                }
            }

            strcpy(hashFromPassword, md5(readPassword, readPasswordLength));

            for(int stringAddress = 0; stringAddress < PASS_LEN + 1; stringAddress++)
            {
                if(target[stringAddress] == '\n')
                {
                    target[stringAddress] = '\0';
                    givenHashLength = stringAddress;
                    break;
                }
            }

            if(strcmp(target, hashFromPassword) == 0)
            {
                isPasswordFound = true;
                break;
            }
        }
        passwordToReturn = readPassword;
        fclose(dictionaryPointer);

        if (!isPasswordFound)
        {
            printf("No matching password found.\n\n");
            return "\0";
        }
            else
            {
                return passwordToReturn;
            }
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
        else
        {
            char currentReadHash[HASH_LEN];
            char foundPassword[PASS_LEN];
            char findPassword[PASS_LEN]; // Not a great variable name.
            char enteredHashFilename[127];
            char enteredDictionaryFilename[127];

            strcpy(enteredHashFilename, argv[1]);
            strcpy(enteredDictionaryFilename, argv[2]);

            FILE *pointerToHashFile;
            FILE *pointerToDictionaryFile;

            if ((pointerToHashFile = fopen(enteredHashFilename, "r")) == NULL)
            {
                printf("Hash file could not be found. Please enter a correct filename.\n");
                return 1;
            }

                else if ((pointerToDictionaryFile = fopen(enteredDictionaryFilename, "r")) == NULL)
                {
                    printf("Dictionary file could not be found. Please enter a correct filename.\n");
                    return 2;
                }
                    else
                    {
                        fclose(pointerToDictionaryFile);

                        while (!feof(pointerToHashFile))
                        {
                            fgets(currentReadHash, HASH_LEN, pointerToHashFile);

                            if(strcmp(currentReadHash, "\0") != 0)
                            {
                                printf("Current hash: %s\n", currentReadHash);

                                strcpy(foundPassword, crackHash(currentReadHash, enteredDictionaryFilename, findPassword));

                                if(strcmp(foundPassword, "\0") != 0)
                                {
                                    printf("Matched password: %s\n\n", foundPassword);
                                }
                            }
                        }
                        fclose(pointerToHashFile);
                    }
        }
}