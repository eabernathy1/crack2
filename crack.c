#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    char readPassword[PASS_LEN + 1];
    char hashFromPassword[HASH_LEN + 1];

    bool isPasswordFound = false;
    
    // Open the dictionary file
    FILE *dictionaryPointer = fopen(dictionary, "r");

    // Loop through the dictionary file, one line
    // at a time.
    while (!feof(dictionaryPointer))
        {
            fgets(readPassword, PASS_LEN, dictionaryPointer);

            strcpy(hashFromPassword, md5(readPassword, PASS_LEN));

            if(strcmp(target, hashFromPassword) == 0)
            {
                //printf("Matched password: %s\n\n", readPassword);
                isPasswordFound = true;
                break;
            }
        }
        fclose(dictionaryPointer);

        if (!isPasswordFound)
        {
            printf("No matching password found.\n\n");
            return NULL;
        }
            else
            {
                return readPassword;
            }
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
        else
        {
            char currentReadHash[HASH_LEN + 1];
            char foundPassword[PASS_LEN + 1];
            char enteredHashFilename[127];
            char enteredDictionaryFilename[127];

            strcpy(enteredHashFilename, argv[1]);
            strcpy(enteredDictionaryFilename, argv[2]);

            FILE *pointerToHashFile;
            FILE *pointerToDictionaryFile;

            //int totalHashesRead = 0;
            //int crackedHashes = 0;

            if ((pointerToHashFile = fopen(enteredHashFilename, "r")) == NULL)
            {
                printf("Hash file could not be found. Please enter a correct filename.\n");
                return 1;
            }

                else if ((pointerToDictionaryFile = fopen(enteredDictionaryFilename, "r")) == NULL)
                {
                    printf("Dictionary file could not be found. Please enter a correct filename.\n");
                    return 2;
                }
                    else
                    {
                        fclose(pointerToDictionaryFile);    

                        while (!feof(pointerToHashFile))
                        {
                            fgets(currentReadHash, HASH_LEN, pointerToHashFile);

                            if(strcmp(currentReadHash, "\0") != 0)
                            {
                                printf("Current hash: %s\n", currentReadHash);

                                strcpy(foundPassword, crackHash(currentReadHash, enteredDictionaryFilename));

                                if(strcmp(foundPassword, "\0") != 0)
                                {
                                    printf("Matched password: %s\n\n", foundPassword);
                                }
                                    else
                                    {
                                        printf("No password found for this hash.");
                                    }
                                //totalHashesRead++;
                            }
                        }
                        fclose(pointerToHashFile);    
                    }
    
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    
    // Free up any malloc'd memory?
        }
}
